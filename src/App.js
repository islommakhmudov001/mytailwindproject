import Header from "./components/Header.js"
import Main from "./components/Main.js"
// import Popular from "./components/Popular/Popular.js"
import Furniture from "./components/Furniture.js"
import Footer from "./components/Footer.js"

function App () {
  return (
    <div className="App w-full flex flex-col items-center justify-center">
      <Header />
      <Main />
      {/* <Popular /> */}
      <Furniture />
      <Footer />
    </div>
  );
}

export default App;
